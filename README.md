# lzq.admin

#### 介绍
Golang语言实现的多租户SaaS系统，项目基于gin、xorm、jwt、swagger、vue、vue-element-admin、element-ui实现的前后端分离多租户后台管理系统，主要功能有多租户、公司、部门、用户管理、RBAC权限管理、按钮级别的权限校验及redis缓存、七牛云存储、接口请求日志、登录日志等，日志支持Elasticsearch

#### 演示
* 地址：http://vue.lzqit.cn/
#### 文档地址：http://docs.lzqit.cn/

#### 功能
1. 多租户：同套代码，同套部署，根据租户进行数据隔离
2. 公司、部门
3. 系统用户管理
4. 菜单管理
5. 操作权限管理：支持菜单、按钮级别的权限校验
6. 角色管理：支持账号多角色授权
7. 日志系统：支持本地日志文件和Elasticsearch
8. 配置：七牛云存储等各种配置
9. 接口请求日志、登录日志
10. 字典功能

#### 交流群
1. QQ群：823184304

#### 项目结构
### 后端

```text
lzq-admin     
├── application             // 应用层
├── config                  // 配置
│   └── appsettings         	// 租户配置
├── docs                    // swagger文档
├── domain      		    // 领域层
|   │── domainconsts    	// 领域常量
|   │── domainservice   	// 领域服务
|   │── dto   				// 应用服务dto
|   └── model   				// 领域实体及应用服务dto
|       └── extrastruct   			// sysconfig配置表中扩展字段的结构体
├── logs      				// 系统日志
├── middleware       		// 中间件
├── pkg      				// 公共库：如orm、auditlog、jwt、redis、logger等
├── router       			// 路由
├── Dockerfile       		// docker容器部署脚本
├── go.mod       			// 应用的第三方开源组件
└── main.go       			// 程序启动入口
```

### 前端
```text
lzq-admin-vue
├── build                      # 构建相关
├── plop-templates             # 基本模板
├── public                     # 静态资源
│   │── favicon.ico            # favicon图标
│   └── index.html             # html模板
├── src                        # 源代码
│   ├── api                    # 所有后端请求接口
│   ├── assets                 # 主题、字体、图片等静态资源
│   ├── components             # 全局公用组件
│   ├── directive              # 全局指令
│   ├── filters                # 全局 filter
│   ├── icons                  # 项目所有 svg icons
│   ├── layout                 # 全局 layout
│   ├── router                 # 路由
│   ├── store                  # 全局 store管理
│   ├── styles                 # 全局样式
│   ├── utils                  # 全局公用方法
│   ├── vendor                 # 公用vendor
│   ├── views                  # views 所有页面
│   ├── App.vue                # 入口页面
│   ├── main.js                # 入口文件 加载组件 初始化等
│   └── permission.js          # 权限管理
├── tests                      # 测试
├── .env.xxx                   # 环境变量配置
├── .eslintrc.js               # eslint 配置项
├── .babelrc                   # babel-loader 配置
├── .travis.yml                # 自动化CI配置
├── vue.config.js              # vue-cli 配置
├── postcss.config.js          # postcss 配置
└── package.json               # package.json
```


#### 安装教程
##### 开发环境搭建
1. 安装MySql数据库>=5.7（必须）
2. 安装Redis数据库（必须）
3. 安装ELK
4. 安装Node（必须）

##### 后端
1. 安装golang及搭建golang开发环境
2. 安装Goland或VisualStudio Code开发工具
3. 复制tools目录下的hsf_basic_dev.sql脚本到自己的mysql数据库中执行
4. 使用golang开发工具运行代码或者进入项目的lzq-admin目录中，使用操作系统命令方式运行golang项目(go run main.go)

##### 前端
```
1. cd lzq-admin-vue
2. npm install --registry=https://registry.npm.taobao.org
3. npm run dev
```
##### 数据库
1. 建好mysql数据库，创建一个utf8的数据库
2. 在配置文件中配置好数据库信息
3. 生产数据库及表格：将配置文件中的database-》is_migration改成true，表示ORM根据当前model结构体和数据库对比，执行更新数据库表格的脚本
4. 初始化数据库脚本表格，根据文件夹tools-》database-init中的sql文件初始化数据

#### 可能出现的问题及解决办法
##### 1. 报node-sass错误
解决办法：
``` 
npm config set sass_binary_site=https://npm.taobao.org/mirrors/node-sass 
```

#### 界面展示
![](/tools/md-images/1646896434.png)
![](/tools/md-images/1646896535.jpg)
![](/tools/md-images/1646896551.jpg)
![](/tools/md-images/1654616733315.jpg)
![](/tools/md-images/1654616495767.jpg)
![](/tools/md-images/1653234523(1).jpg)
![](/tools/md-images/16532343051.jpg)
![](/tools/md-images/16532342055.jpg)
![](/tools/md-images/16532343631.jpg)
![](/tools/md-images/16532343631.jpg)




