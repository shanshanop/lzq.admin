package middleware

import (
	"lzq-admin/config"
	"lzq-admin/config/appsettings"
	"lzq-admin/domain/domainconsts"
	token "lzq-admin/pkg/auth"
	"lzq-admin/pkg/tenant"
	"net/http"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/3/11
 * @Version 1.0.0
 */

func CheckAuth() gin.HandlerFunc {
	return func(c *gin.Context) {
		accessToken := c.GetHeader("Authorization")
		if accessToken == "" {
			c.JSON(http.StatusUnauthorized, gin.H{"msg": "未登陆系统"})
			c.Abort()
			return
		} else {
			if strings.HasPrefix(accessToken, "Bearer ") {
				tokenClaims := token.GetClaims(c)
				if len(tokenClaims.Id) <= 0 {
					c.JSON(http.StatusOK, gin.H{"code": "50001", "msg": "token无效"})
					c.Abort()
					return
				}

				if time.Now().Unix() > tokenClaims.ExpiresAt {
					c.JSON(http.StatusOK, gin.H{"msg": "Token已超时"})
					c.Abort()
					return
				}
				// 校验租户
				if config.Config.UseMultiTenancy {
					var tenantInfo appsettings.TenantInfo
					var err error
					tenantInfo, err = tenant.GetTenantById(tokenClaims.TenantId)
					if err != nil {
						c.JSON(http.StatusOK, gin.H{"code": "50001", "msg": err.Error()})
						c.Abort()
						return
					}
					if tenantInfo.Status != domainconsts.TenantStatusEnable {
						c.JSON(http.StatusOK, gin.H{"code": "50001", "msg": "租户已" + domainconsts.GetConstFlag(tenantInfo.Status, domainconsts.TenantConstFlags)})
						c.Abort()
						return
					}
				}
			} else {
				c.JSON(http.StatusOK, gin.H{"code": "50001", "msg": "Token开通必须以Bearer+空格开头"})
				c.Abort()
				return
			}
		}

		c.Next()
	}
}
