/*
 * @Author: 糊涂的老知青
 * @Date: 2022-07-30
 * @Version: 1.0.0
 * @Description:
 */
package middleware

import (
	"bytes"
	"io/ioutil"
	gconfig "lzq-admin/config"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	infra_model "lzq-admin/domain/model/infrastructure"
	infra_extrastruct "lzq-admin/domain/model/infrastructure/extrastruct"
	"lzq-admin/pkg/hsflogger"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	jsoniter "github.com/json-iterator/go"
)

/**
 * @Author  糊涂的老知青
 * @Date    2021/12/18
 * @Version 1.0.0
 */

func LogAuditLogAction() gin.HandlerFunc {
	return func(c *gin.Context) {
		start := time.Now()
		c.Request.Header.Set("RequestTime", strconv.FormatInt(start.UnixMilli(), 10))
		if strings.Contains(c.Request.URL.Path, "auth/login") {
			c.Next()
		} else {
			config, err := infra_domainservice.NewDSSysConfig(c).GetSysConfigCacheByCode(infra_model.ExtraGlobalConfig, "SystemGlobalConfig")
			if err != nil {
				hsflogger.LogError("获取系统全局配置异常："+err.Error(), err)
			} else {
				var globalConfig infra_extrastruct.SystemGlobalConfig
				if err := jsoniter.UnmarshalFromString(config, &globalConfig); err != nil {
					hsflogger.LogError("解析系统全局配置异常："+err.Error(), err)
				} else {
					if globalConfig.UseAuditLogAction {
						var logAuditLogAction infra_model.CreateLogAuditLogActionDto
						if len(c.Request.URL.RawQuery) > 0 {
							logAuditLogAction.Parameters = c.Request.URL.RawQuery
						} else {
							if !strings.Contains(c.Request.URL.Path, "sysUser/updateCurrentUserPassword") &&
								!strings.Contains(c.Request.URL.Path, "sysUser/editUserPassword") &&
								!strings.Contains(c.Request.URL.Path, "sysUser/sysUser") {
								data, _ := c.GetRawData()
								if len(data) > 0 {
									c.Request.Body = ioutil.NopCloser(bytes.NewBuffer(data)) // 解决gin只能读取一次body的问题
									logAuditLogAction.Parameters = string(data)
								}
							}
						}
						defer func() {
							logAuditLogAction.HTTPMethod = c.Request.Method
							logAuditLogAction.URL = c.Request.URL.Path
							logAuditLogAction.BrowserInfo = c.Request.Header.Get("User-Agent")
							logAuditLogAction.ExecutionTime = start
							logAuditLogAction.ExecutionDuration = time.Since(start).Milliseconds()
							logAuditLogAction.HTTPStatusCode = c.Writer.Status()
							logAuditLogAction.ActionType = "Other"
							logAuditLogAction.FromSource = gconfig.Config.ServiceModuleCode
							logAuditLogAction.ClientIPAddress = c.ClientIP()
							if len(c.Request.Header.Get("X-Forward-For")) > 0 {
								logAuditLogAction.ClientIPAddress = c.Request.Header.Get("X-Forward-For")
							}
							if err := recover(); err != nil {
								logAuditLogAction.Exceptions, _ = jsoniter.MarshalToString(err)
							}
							requestParams := make(map[string]interface{})
							requestParams["fields.Request.URL"] = c.Request.URL
							requestParams["fields.Request.Host"] = c.Request.Host
							requestParams["fields.Request.ContentLength"] = c.Request.ContentLength
							requestParams["fields.Request.Header"] = c.Request.Header
							logAuditLogAction.ExtraProperties = requestParams
							_ = infra_domainservice.NewDSLogAuditLogAction(c).Insert(logAuditLogAction)

							// tt, _ := jsoniter.MarshalToString(logAuditLogAction)
							// eventbus.Distributed.PublishCommand("ActiveTest", "EB.Auditlog", tt, "")
						}()
					}
				}
			}
			c.Next()
		}
	}
}
