package cache

import (
	"fmt"
	"time"

	"github.com/gin-gonic/gin"
	uuid "github.com/satori/go.uuid"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/2/25
 * @Version 1.0.0
 */

type cacheHelper struct{}

var LzqCacheHelper = &cacheHelper{}

// GetCacheVersion 获取缓存类型版本号
func (c *cacheHelper) GetCacheVersion(ctx *gin.Context, cacheType string) string {
	key := c.GetCacheTypeVersionKey(cacheType)
	value := RedisUtil.NewRedis(ctx, false).Get(key)
	if len(value) == 0 {
		value = uuid.NewV4().String()
		RedisUtil.NewRedis(ctx, false).Set(key, value, time.Hour*24)
	}
	return value
}
func (c *cacheHelper) GetCacheTypeVersionKey(cacheType string) string {
	return fmt.Sprintf("CacheVersion:%v", cacheType)
}
