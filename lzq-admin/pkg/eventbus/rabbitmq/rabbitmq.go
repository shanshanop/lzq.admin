/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-13
 * @Version: 1.0.0
 * @Description:
 */
package rabbitmq

import (
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

type LzqRabbitMQ struct {
	conn      *amqp.Connection
	channel   *amqp.Channel
	QueueName string //队列名称
	Exchange  string //交换机名称
	Key       string //bind Key 名称
	Mqurl     string //连接信息
}

func NewRabbitMQ(mqurl, queueName, exchange, key string) *LzqRabbitMQ {
	return &LzqRabbitMQ{Mqurl: mqurl, QueueName: queueName, Exchange: exchange, Key: key}
}

// 错误处理函数
func (r *LzqRabbitMQ) failOnErr(err error, message string) {
	if err != nil {
		log.Fatalf("%s:%s", message, err)
		panic(fmt.Sprintf("%s:%s", message, err))
	}
}
