package router

/**
 * @Author  糊涂的老知青
 * @Date    2021/10/30
 * @Version 1.0.0
 */

import (
	infra_application "lzq-admin/application/infrastructure"
	"lzq-admin/middleware"

	"github.com/gin-gonic/gin"
)

// AdminRouter 系统路由
func AdminRouter(router *gin.RouterGroup) {
	router.Use(middleware.CheckJwtToken())
	{
		authRouter := router.Group("/auth").Use()
		{
			authRouter.GET("/captcha", infra_application.IAuthAppService.GetCaptcha)
			authRouter.POST("/login", infra_application.IAuthAppService.Login)
			authRouter.Use(middleware.CheckAuth()).POST("/logOut", infra_application.IAuthAppService.Logout)
		}

		sysConfigRouter := router.Group("/sysconfig").Use(middleware.CheckAuth())
		{
			sysConfigRouter.GET("/getSysConfigCache", infra_application.ISystemConfigAppService.GetSysConfigJsonMapCache)
			sysConfigRouter.POST("/qnupdate", infra_application.ISystemConfigAppService.QiuNiuUpdate)
			sysConfigRouter.GET("/getInfo", infra_application.ISystemConfigAppService.GetInfo)
			sysConfigRouter.POST("/create", infra_application.ISystemConfigAppService.Create)
		}

		systemUserRouter := router.Group("/sysUser").Use(middleware.CheckAuth())
		{
			systemUserRouter.POST("/sysUser", infra_application.ISystemUserAppService.Create)
			systemUserRouter.GET("/get", infra_application.ISystemUserAppService.Get)
			systemUserRouter.DELETE("/user", infra_application.ISystemUserAppService.Delete)
			systemUserRouter.GET("/sysUserList", infra_application.ISystemUserAppService.GetList)
			systemUserRouter.POST("/editSysUser", infra_application.ISystemUserAppService.Update)
			systemUserRouter.GET("/userInfo", infra_application.ISystemUserAppService.GetUserInfo)
			systemUserRouter.PUT("/sysUserStatus", infra_application.ISystemUserAppService.UpdateSystemStatus)
			systemUserRouter.POST("/editUserPassword", infra_application.ISystemUserAppService.UpdateSystemUserPassword)
			systemUserRouter.GET("/defaultAvatar", infra_application.ISystemUserAppService.GetDefaultAvatar)
			systemUserRouter.GET("/currentUserInfo", infra_application.ISystemUserAppService.GetCurrentUserInfo)
			systemUserRouter.POST("/updateCurrentUserPassword", infra_application.ISystemUserAppService.UpdateCurrentUserPassword)
		}

		systemFileRouter := router.Group("/sysfile").Use(middleware.CheckAuth())
		{
			systemFileRouter.POST("/upload", infra_application.ISysFileAppService.Upload)
			systemFileRouter.POST("/batchUpload", infra_application.ISysFileAppService.BatchUpload)
		}

		authModuleRouter := router.Group("/authModule").Use(middleware.CheckAuth())
		{
			authModuleRouter.POST("/create", infra_application.IAuthModuleAppService.Create)
			authModuleRouter.GET("/get", infra_application.IAuthModuleAppService.Get)
			authModuleRouter.DELETE("/delete", infra_application.IAuthModuleAppService.Delete)
			authModuleRouter.GET("/list", infra_application.IAuthModuleAppService.GetList)
			authModuleRouter.PUT("/update", infra_application.IAuthModuleAppService.Update)
		}

		authMenuRouter := router.Group("/menu").Use(middleware.CheckAuth())
		{
			authMenuRouter.POST("/menu", infra_application.IAuthMenuAppService.Create)
			authMenuRouter.GET("/get", infra_application.IAuthMenuAppService.Get)
			authMenuRouter.DELETE("/menu", infra_application.IAuthMenuAppService.Delete)
			authMenuRouter.GET("/menusList", infra_application.IAuthMenuAppService.GetList)
			authMenuRouter.PUT("/menu", infra_application.IAuthMenuAppService.Update)
			authMenuRouter.GET("/menuList", infra_application.IAuthMenuAppService.GetMenuList)
		}

		authPermissionRouter := router.Group("/permission").Use(middleware.CheckAuth())
		{
			authPermissionRouter.POST("/permission", infra_application.IAuthPermissionAppService.Create)
			authPermissionRouter.GET("/get", infra_application.IAuthPermissionAppService.Get)
			authPermissionRouter.DELETE("/permission", infra_application.IAuthPermissionAppService.Delete)
			authPermissionRouter.GET("/data", infra_application.IAuthPermissionAppService.GetList)
			authPermissionRouter.PUT("/permission", infra_application.IAuthPermissionAppService.Update)
			authPermissionRouter.GET("/permissionGroup", infra_application.IAuthPermissionAppService.GetPermissionGroup)
		}

		authRoleRouter := router.Group("/role").Use(middleware.CheckAuth())
		{
			authRoleRouter.POST("/role", infra_application.IAuthRoleAppService.Create)
			authRoleRouter.GET("/get", infra_application.IAuthRoleAppService.Get)
			authRoleRouter.DELETE("/role", infra_application.IAuthRoleAppService.Delete)
			authRoleRouter.GET("/roleList", infra_application.IAuthRoleAppService.GetList)
			authRoleRouter.PUT("/role", infra_application.IAuthRoleAppService.Update)
			authRoleRouter.PUT("/roleStatus", infra_application.IAuthRoleAppService.UpdateRoleStatus)
			authRoleRouter.GET("/roles", infra_application.IAuthRoleAppService.GetEnanleRoles)
		}

		authCheckerRouter := router.Group("/authenticateChecker").Use(middleware.CheckAuth())
		{
			authCheckerRouter.GET("/grantedMenus", infra_application.IAuthCheckerAppService.GetGrantedMenus)
		}

		authorizeRouter := router.Group("/authorize").Use(middleware.CheckAuth())
		{
			authorizeRouter.GET("/rolePermissionDatas/:roleId", infra_application.IAuthRoleAppService.GetRolePermissionDatas)
			authorizeRouter.POST("/grantPermissions", infra_application.IAuthRoleAppService.GrantPermissions)
			authorizeRouter.DELETE("/userRole", infra_application.IAuthCheckerAppService.DeleteUserRole)
		}
		permissionCheckerRouter := router.Group("/permissionChecker").Use(middleware.CheckAuth())
		{
			permissionCheckerRouter.GET("/grantedPermissions", infra_application.IAuthCheckerAppService.GetCurrentUserGrantedPermissions)
		}

		auditLogActionRouter := router.Group("/auditLogAction").Use(middleware.CheckAuth())
		{
			auditLogActionRouter.GET("/list", infra_application.LogAuditLogActionAppService.GetList)
			auditLogActionRouter.GET("/currentUserLogsList", infra_application.LogAuditLogActionAppService.GetCurrentUserLogsList)
		}

		companyRouter := router.Group("/company").Use(middleware.CheckAuth())
		{
			companyRouter.POST("/create", infra_application.SystemCompanyAppService.Create)
			companyRouter.PUT("/update", infra_application.SystemCompanyAppService.Update)
			companyRouter.DELETE("/delete", infra_application.SystemCompanyAppService.Delete)
		}
		deptRouter := router.Group("/dept").Use(middleware.CheckAuth())
		{
			deptRouter.POST("/create", infra_application.SystemDeptAppService.Create)
			deptRouter.PUT("/update", infra_application.SystemDeptAppService.Update)
			deptRouter.DELETE("/delete", infra_application.SystemDeptAppService.Delete)
			deptRouter.GET("/companyAndDeptList", infra_application.SystemDeptAppService.GetCompanyAndDeptList)
		}
		dictRouter := router.Group("/systemDictionary").Use(middleware.CheckAuth())
		{
			dictRouter.POST("/createDict", infra_application.SystemDictionaryAppService.CreateDict)
			dictRouter.POST("/create", infra_application.SystemDictionaryAppService.Create)
			dictRouter.PUT("/update", infra_application.SystemDictionaryAppService.Update)
			dictRouter.DELETE("/delete", infra_application.SystemDictionaryAppService.Delete)
			dictRouter.GET("/list", infra_application.SystemDictionaryAppService.GetList)
			dictRouter.PUT("/updateStatus", infra_application.SystemDictionaryAppService.UpdateStatus)
			dictRouter.POST("/refresh", infra_application.SystemDictionaryAppService.RefreshSystemDictCache)
			dictRouter.GET("/getDictsByCode", infra_application.SystemDictionaryAppService.GetDictsByDictCode)
		}

	}
}
