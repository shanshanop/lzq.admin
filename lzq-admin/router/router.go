/*
 * @Author: 糊涂的老知青
 * @Date: 2022-05-22
 * @Version: 1.0.0
 * @Description:
 */
package router

import (
	_ "lzq-admin/docs"
	"lzq-admin/middleware"

	"github.com/gin-gonic/gin"
	ginSwagger "github.com/swaggo/gin-swagger"
	"github.com/swaggo/gin-swagger/swaggerFiles"
)

func Init() *gin.Engine {
	router := gin.Default()
	router.Use(middleware.Cors())
	router.GET("/swagger/*any", ginSwagger.WrapHandler(swaggerFiles.Handler))
	router.Use(middleware.LogAuditLogAction())

	/***基础服务****/
	infraRouter := router.Group("/api/app")
	AdminRouter(infraRouter)

	/***租户管理****/
	tenantRouter := router.Group("/api/tenant")
	TenantRouter(tenantRouter)

	/***工作流****/
	workflowRouter := router.Group("/api/workflow")
	WorkflowRouter(workflowRouter)

	// 消费事件
	//eventbus.Distributed.ComsumerCommand("ActiveTest")
	return router
}
