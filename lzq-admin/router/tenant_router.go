/*
 * @Author: 糊涂的老知青
 * @Date: 2022-07-30
 * @Version: 1.0.0
 * @Description:
 */
package router

/**
 * @Author  糊涂的老知青
 * @Date    2021/10/30
 * @Version 1.0.0
 */

import (
	application "lzq-admin/application/tenant"
	"lzq-admin/middleware"

	"github.com/gin-gonic/gin"
)

// TenantRouter 认证路由
func TenantRouter(router *gin.RouterGroup) {

	router.Use()
	{
		tenantRouter := router.Group("/tenant").Use(middleware.CheckAuth())
		{
			tenantRouter.POST("/create", application.ITenantAppService.Create)
		}
	}
}
