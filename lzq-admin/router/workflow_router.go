/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */
package router

import (
	"lzq-admin/application"
	"lzq-admin/middleware"

	"github.com/gin-gonic/gin"
)

func WorkflowRouter(router *gin.RouterGroup) {
	router.Use(middleware.CheckJwtToken())
	{
		testRouter := router.Group("/designer").Use()
		{
			testRouter.GET("/testReflectSetValue", application.ITestAppService.TestReflectSetValue)
		}

	}
}
