/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-20
 * @Version: 1.0.0
 * @Description:
 */
package infra_model

const (
	TableSystemConfig             = "sys_config"
	TableSystemUser               = "sys_user"
	TableSystemCompany            = "sys_company"
	TableSystemDept               = "sys_dept"
	TableSystemDictionary         = "sys_dictionary"
	TableAuthModule               = "au_module"
	TableAuthMenu                 = "au_menu"
	TableAuthPermission           = "au_permission"
	TableAuthRole                 = "au_role"
	TableAuthRolePermission       = "au_rolepermission"
	TableAuthUserdataprivilege    = "au_userdataprivilege"
	TableSystemFile               = "sys_file"
	TableLogAuditLog              = "log_auditlog"
	TableLogEntityChanges         = "log_entitychanges"
	TableLogEntityPropertyChanges = "log_entitypropertychanges"
	TableLogAuditLogAction        = "log_auditlogaction"
)
