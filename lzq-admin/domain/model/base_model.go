/*
 * @Author: 糊涂的老知青
 * @Date: 2021-10-30
 * @Version: 1.0.0
 * @Description:
 */
package model

import (
	"time"
)

// BaseModel 基类
type BaseModel struct {
	ID                   string    `json:"id" xorm:"pk char(36) comment('主键')"` //主键
	CreationTime         time.Time `json:"-" xorm:"created notnull comment('创建时间')"`
	CreatorId            string    `json:"-" xorm:"char(36) comment('创建人')"`
	LastModificationTime time.Time `json:"-" xorm:"comment('最后修改时间')"`
	LastModifierId       string    `json:"-" xorm:"char(36) comment('最后修改人')"`
	IsDeleted            bool      `json:"-" xorm:"bool default(0) comment('是否已删除')"`
	DeleterId            string    `json:"-" xorm:"char(36) comment('删除人')"`
	DeletionTime         time.Time `json:"-" xorm:"comment('删除时间')"`
}

// TenantBaseModel 租户字段基类
type TenantBaseModel struct {
	TenantId string `json:"-" xorm:"char(36) comment('租户ID')"` //租户ID
}

// HasExtraPropertiesBaseModel 扩展字段基类
type HasExtraPropertiesBaseModel struct {
	ExtraProperties map[string]interface{} `json:"-" xorm:"json longtext comment('扩展字段')"`
}
