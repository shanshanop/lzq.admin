package infra_domainservice

import (
	"lzq-admin/domain/domainservice"
	"lzq-admin/pkg/cache"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/2/28
 * @Version 1.0.0
 */

type AuthPrivilegeCacheService struct {
	domainservice.BaseDomainService
}

func NewDSAuthPrivilege(c *gin.Context) *AuthPrivilegeCacheService {
	return &AuthPrivilegeCacheService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *AuthPrivilegeCacheService) DeleteFunctionPrivilegeCache() {
	cache.RedisUtil.NewRedis(s.GinCtx, false).Delete(cache.LzqCacheHelper.GetCacheTypeVersionKey(cache.LzqCacheTypeFunctionPrivilege))
}

func (s *AuthPrivilegeCacheService) DeleteRoleGrantedPermissionsCache(roleIds ...string) {
	for _, v := range roleIds {
		cache.RedisUtil.NewRedis(s.GinCtx, true).Delete(cache.NewLzqCacheKey(s.GinCtx).GetRoleGrantedPermissionsCacheKey(v))
	}
}

func (s *AuthPrivilegeCacheService) DeleteDataPrivilegeCache() {
	cache.RedisUtil.NewRedis(s.GinCtx, true).Delete(cache.LzqCacheHelper.GetCacheTypeVersionKey(cache.LzqCacheTypeDataPrivilege))
}

func (s *AuthPrivilegeCacheService) DeleteUserAuthCache(userId string) {
	cache.RedisUtil.NewRedis(s.GinCtx, true).Delete(cache.NewLzqCacheKey(s.GinCtx).GetUserGrantedMenusCacheKey(userId))
	cache.RedisUtil.NewRedis(s.GinCtx, true).Delete(cache.NewLzqCacheKey(s.GinCtx).GetGrantedDataPrivilegeByUserCacheKey(userId))
	cache.RedisUtil.NewRedis(s.GinCtx, true).Delete(cache.NewLzqCacheKey(s.GinCtx).GetUserGrantedPermissionsCacheKey(userId))
}
