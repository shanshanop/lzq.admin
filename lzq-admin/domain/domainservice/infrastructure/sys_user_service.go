package infra_domainservice

/**
 * @Author  糊涂的老知青
 * @Date    2021/11/05
 * @Version 1.0.0
 */

import (
	"fmt"
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/cache"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"

	"github.com/gin-gonic/gin"
	jsoniter "github.com/json-iterator/go"
	"github.com/pkg/errors"
	"golang.org/x/crypto/bcrypt"
)

// SystemUserDomainService 系统用户领域服务
type SystemUserDomainService struct {
	domainservice.BaseDomainService
}

func NewDSSystemUser(c *gin.Context) *SystemUserDomainService {
	return &SystemUserDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (u *SystemUserDomainService) Insert(modelDto infra_model.CreateSystemUserDto) (result infra_model.SystemUser, err error) {
	var entity infra_model.SystemUser
	var exist bool
	lzqOrm := orm.NewLzqOrm(u.GinCtx)
	exist, err = lzqOrm.QSession(true).Where("LoginName=?", modelDto.LoginName).Exist(&infra_model.SystemUser{})
	if err != nil {
		return entity, err
	}
	if exist {
		return entity, errors.New("登陆名：" + modelDto.LoginName + " 已存在，请更换登录名")
	}
	entity = infra_model.SystemUser{
		SystemUserBase: infra_model.SystemUserBase{
			LoginName:  modelDto.LoginName,
			UserName:   modelDto.UserName,
			Email:      modelDto.Email,
			HeadImgURL: modelDto.HeadImgURL,
			Sex:        modelDto.Sex,
			Mobile:     modelDto.Mobile,
			CompanyId:  modelDto.CompanyId,
			DeptId:     modelDto.DeptId,
		},
	}
	entity.ID = utility.UuidCreate()
	entity.Status = domainconsts.SystemUserStatusEnable

	if modelDto.Password != modelDto.SurePassword {
		return infra_model.SystemUser{}, errors.New("两次输入的密码不同，请重新输入")
	}
	// 密码加密不可解析密码串
	phash, err1 := bcrypt.GenerateFromPassword([]byte(modelDto.Password), bcrypt.DefaultCost)
	if err1 != nil {
		return entity, err1
	}
	entity.Password = string(phash)
	dbSession, errT := lzqOrm.BeginTrans()
	if errT != nil {
		return infra_model.SystemUser{}, err
	}

	if _, err := NewDSAuthUserdataPrivilege(u.GinCtx).Insert(dbSession, entity.ID, modelDto.RoleIds); err != nil {
		dbSession.Rollback()
		return infra_model.SystemUser{}, err
	}
	if _, err := lzqOrm.ISessionWithTrans(dbSession).Insert(&entity); err != nil {
		dbSession.Rollback()
		return infra_model.SystemUser{}, err
	}
	if err := dbSession.Commit(); err != nil {
		dbSession.Rollback()
		return infra_model.SystemUser{}, err
	}
	return entity, nil
}

func (u *SystemUserDomainService) Update(inputDto infra_model.UpdateSystemUserDto) (infra_model.SystemUser, error) {
	var user infra_model.SystemUser
	lzqOrm := orm.NewLzqOrm(u.GinCtx)
	if has, err := lzqOrm.QSession(true).ID(inputDto.ID).Get(&user); err != nil {
		return infra_model.SystemUser{}, err
	} else if !has {
		return infra_model.SystemUser{}, errors.New("用户不存在")
	}

	user.UserName = inputDto.UserName
	user.Email = inputDto.Email
	user.HeadImgURL = inputDto.HeadImgURL
	user.Mobile = inputDto.Mobile
	user.CompanyId = inputDto.CompanyId
	user.DeptId = inputDto.DeptId
	if _, ok := domainconsts.SystemUserSexConstFlags[inputDto.Sex]; ok {
		user.Sex = inputDto.Sex
	} else {
		return infra_model.SystemUser{}, errors.New("性别类型：" + inputDto.Sex + "不存在")
	}

	dbSession, errT := lzqOrm.BeginTrans()
	if errT != nil {
		return infra_model.SystemUser{}, errT
	}
	if len(inputDto.RoleIds) == 0 {
		if err := NewDSAuthUserdataPrivilege(u.GinCtx).Delete(dbSession, inputDto.ID, ""); err != nil {
			dbSession.Rollback()
			return infra_model.SystemUser{}, err
		}
	} else {
		if len(inputDto.RoleIds) > 1 || len(inputDto.RoleIds) == 1 && inputDto.RoleIds[0] != "00000000-0000-0000-0000-000000000000" {
			if _, err := NewDSAuthUserdataPrivilege(u.GinCtx).Insert(dbSession, inputDto.ID, inputDto.RoleIds); err != nil {
				dbSession.Rollback()
				return infra_model.SystemUser{}, err
			}
		}
	}

	if updateNum, err1 := lzqOrm.USessionWithTrans(true, dbSession).AllCols().ID(inputDto.ID).Update(&user); err1 != nil {
		return infra_model.SystemUser{}, err1
	} else if updateNum < 1 {
		return infra_model.SystemUser{}, errors.New("修改失败")
	}
	dbSession.Commit()
	// 清除用户详情缓存
	u.RemoveUserInfoById(inputDto.ID)
	return user, nil
}

func (u *SystemUserDomainService) UpdateSystemUserPassword(inputDto infra_model.UpdateSystemUserPasswordDto) error {
	var user infra_model.SystemUser
	lzqOrm := orm.NewLzqOrm(u.GinCtx)
	if has, err := lzqOrm.QSession(true).ID(inputDto.ID).Get(&user); err != nil {
		return err
	} else if !has {
		return errors.New("用户不存在")
	}

	if err := bcrypt.CompareHashAndPassword([]byte(user.Password), []byte(inputDto.Password)); err != nil {
		return errors.New("旧密码不正确")
	}

	if inputDto.SurePassword != inputDto.NewPassword {
		return errors.New("两次输入的密码不相同")
	}

	// 密码加密不可解析密码串
	if phash, err := bcrypt.GenerateFromPassword([]byte(inputDto.NewPassword), bcrypt.DefaultCost); err != nil {
		return err
	} else {
		user.Password = string(phash)
	}
	if effect, err := lzqOrm.USession(true).ID(inputDto.ID).Cols("Password").Update(&user); err != nil {
		return err
	} else if effect <= 0 {
		return errors.New("修改失败")
	} else {
		return nil
	}
}

// Get 根据用户ID或者LoginName查询用户信息
func (u *SystemUserDomainService) Get(m *infra_model.SystemUser, id, loginName string) error {
	var has bool = false
	var err error
	lzqOrm := orm.NewLzqOrm(u.GinCtx)
	if id != "" {
		has, err = lzqOrm.QSession(true).ID(id).Get(m)
	} else if loginName != "" {
		has, err = lzqOrm.QSession(true).Where("LoginName=?", loginName).Get(m)
	}

	if err != nil {
		return err
	}
	if has {
		return nil
	} else {
		return errors.New("账号不存在")
	}
}

func (u *SystemUserDomainService) GetUserInfo(userId string) (infra_model.SystemUserInfoDto, error) {
	key := fmt.Sprintf("%v:%v", cache.LzqCacheHelper.GetCacheVersion(u.GinCtx, cache.LzqCacheTypeSysUser), userId)
	r := cache.RedisUtil.NewRedis(u.GinCtx, true, "UserInfo")
	userJson := r.Get(key)
	var userInfo infra_model.SystemUserInfoDto
	if userJson != "" {
		if err := jsoniter.UnmarshalFromString(userJson, &userInfo); err != nil {
			return userInfo, nil
		}
		return userInfo, nil
	}
	var user infra_model.SystemUser
	if err := u.Get(&user, userId, ""); err != nil {
		return userInfo, err
	}
	userInfo = infra_model.SystemUserInfoDto{
		RoleIds:       nil,
		RoleName:      "",
		Status:        user.Status,
		LoginName:     user.LoginName,
		UserName:      user.UserName,
		HeadImgURL:    user.HeadImgURL,
		HeadImgLink:   user.HeadImgURL,
		Sex:           user.Sex,
		Mobile:        user.Mobile,
		Email:         user.Email,
		IsTenantAdmin: user.IsTenantAdmin,
		ID:            user.ID,
		CompanyId:     user.CompanyId,
		DeptId:        user.DeptId,
	}

	v, isHave := user.ExtraProperties["SuperAdmin"]
	userInfo.SuperAdmin = isHave && v.(bool)

	r.SSet(key, userInfo, 0)
	return userInfo, nil
}

func (u *SystemUserDomainService) IsTenantAdmin(userId string) (bool, error) {
	if userInfo, err := u.GetUserInfo(userId); err != nil {
		return false, err
	} else {
		return userInfo.IsTenantAdmin, nil
	}
}

func (u *SystemUserDomainService) IsSuperAdmin(userId string) (bool, error) {
	if userInfo, err := u.GetUserInfo(userId); err != nil {
		return false, err
	} else {
		return userInfo.SuperAdmin, nil
	}
}

func (u *SystemUserDomainService) RemoveUserInfoById(userId string) {
	key := fmt.Sprintf("%v:%v", cache.LzqCacheHelper.GetCacheVersion(u.GinCtx, cache.LzqCacheTypeSysUser), userId)
	cache.RedisUtil.NewRedis(u.GinCtx, true).Delete(key)
}
