package infra_domainservice

import (
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	"lzq-admin/domain/model"
	infra_model "lzq-admin/domain/model/infrastructure"
	token "lzq-admin/pkg/auth"
	"lzq-admin/pkg/hsflogger"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/pkg/errors"
)

/**
 * @Author  糊涂的老知青
 * @Date    2021/12/2
 * @Version 1.0.0
 */

// AuthMenuDomainService 菜单领域服务
type AuthMenuDomainService struct {
	domainservice.BaseDomainService
	wg sync.WaitGroup
}

func NewDSAuthMenu(c *gin.Context) *AuthMenuDomainService {
	return &AuthMenuDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
		sync.WaitGroup{},
	}
}

func (s *AuthMenuDomainService) Insert(modelDto infra_model.CreateAuthMenuDto) (infra_model.AuthMenu, error) {
	var entity infra_model.AuthMenu
	entity = infra_model.AuthMenu{
		AuthMenuBase: infra_model.AuthMenuBase{
			ModuleId: modelDto.ModuleId,
			Code:     modelDto.Code,
			Name:     modelDto.Name,
			Rank:     modelDto.Rank,
			Policy:   modelDto.Policy,
			Icon:     modelDto.Icon,
			ParentId: modelDto.ParentId,
			IsBranch: modelDto.IsBranch,
			IsHidden: modelDto.IsHidden,
			Path:     modelDto.Path,
			Url:      modelDto.Url,
		},
	}
	entity.ID = utility.UuidCreate()
	var dbSession, err = orm.NewLzqOrm(s.GinCtx).BeginTrans()
	if err != nil {
		hsflogger.LogError("", err)
	}
	if modelDto.IsBranch == false {
		_, err = NewDSAuthPermission(s.GinCtx).Insert(dbSession, infra_model.CreateAuthPermissionDto{
			AuthPermissionBase: infra_model.AuthPermissionBase{
				MenuId:          entity.ID,
				Name:            "页面访问",
				Code:            "Access",
				Rank:            1,
				Policy:          infra_model.DefaultViewPolicy(),
				PermissionGroup: domainconsts.PermissionGroupView,
			},
		})
	}

	_, err = orm.NewLzqOrm(s.GinCtx).ISessionWithTrans(dbSession).Insert(&entity)

	if err != nil {
		dbSession.Rollback()
		return infra_model.AuthMenu{}, err
	}
	err = dbSession.Commit()
	NewDSAuthPrivilege(s.GinCtx).DeleteFunctionPrivilegeCache()
	return entity, err
}

func (s *AuthMenuDomainService) Get(id string) (infra_model.AuthMenuDto, error) {
	var dto infra_model.AuthMenuDto
	has, err := orm.NewLzqOrm(s.GinCtx).QSession(false).ID(id).Get(&dto)
	if err != nil {
		return dto, err
	}
	if has {
		return dto, nil
	} else {
		return dto, errors.New("菜单不存在或已删除")
	}
}

func (s *AuthMenuDomainService) Update(inputDto infra_model.UpdateAuthMenuDto) (infra_model.AuthMenu, error) {
	var m infra_model.AuthMenu
	var has int64
	var err error
	if has, err = orm.NewLzqOrm(s.GinCtx).QSession(false).ID(inputDto.Id).Count(&m); err != nil {
		return infra_model.AuthMenu{}, err
	}
	if has <= 0 {
		return infra_model.AuthMenu{}, errors.New("菜单不存在")
	}
	m.Name = inputDto.Name
	m.Code = inputDto.Code
	m.Rank = inputDto.Rank
	m.IsHidden = inputDto.IsHidden
	m.Path = inputDto.Path
	m.Url = inputDto.Url
	m.IsBranch = inputDto.IsBranch
	m.ParentId = inputDto.ParentId
	m.Icon = inputDto.Icon
	m.ModuleId = inputDto.ModuleId
	m.Policy = inputDto.Policy

	var updateFields []string
	updateFields, err = orm.GetUpdateFields(infra_model.UpdateAuthMenuDto{})
	if err != nil {
		return infra_model.AuthMenu{}, err
	}

	var updateNum int64
	if updateNum, err = orm.NewLzqOrm(s.GinCtx).USession(false).Cols(updateFields...).ID(inputDto.Id).Update(&m); err != nil {
		return infra_model.AuthMenu{}, err
	}
	if updateNum < 1 {
		return infra_model.AuthMenu{}, errors.New("修改失败")
	}
	// TODO 容器菜单改成非容器菜单，需要往AuthPermission表中插入页面访问操作按钮
	NewDSAuthPrivilege(s.GinCtx).DeleteFunctionPrivilegeCache()
	return m, nil
}

func (s *AuthMenuDomainService) Delete(id string) error {
	m := infra_model.AuthMenu{
		BaseModel: model.BaseModel{
			IsDeleted:    true,
			DeleterId:    token.GetCurrentUserName(s.GinCtx),
			DeletionTime: time.Now(),
		},
	}
	dbSession, err := orm.NewLzqOrm(s.GinCtx).BeginTrans()
	if err != nil {
		return err
	}
	s.wg.Add(1)
	var err1 error
	_, err = orm.NewLzqOrm(s.GinCtx).DSessionWithTrans(false, dbSession).ID(id).Update(&m)
	go func() {
		err1 = NewDSAuthPermission(s.GinCtx).Delete(dbSession, id)
		s.wg.Done()
	}()
	s.wg.Wait()
	if err != nil || err1 != nil {
		dbSession.Rollback()
		if err1 != nil {
			err = err1
		}
		return err
	}
	err = dbSession.Commit()
	if err != nil {
		return err
	}
	NewDSAuthPrivilege(s.GinCtx).DeleteFunctionPrivilegeCache()
	return nil
}
