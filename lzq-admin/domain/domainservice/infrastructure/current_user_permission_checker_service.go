/*
 * @Author: 糊涂的老知青
 * @Date: 2022-07-30
 * @Version: 1.0.0
 * @Description:
 */
package infra_domainservice

import (
	"lzq-admin/domain/domainservice"
	token "lzq-admin/pkg/auth"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/2/27
 * @Version 1.0.0
 */

type CurrentUserPermissionChecker struct {
	domainservice.BaseDomainService
}

func NewDSCurrentUserPermission(c *gin.Context) *CurrentUserPermissionChecker {
	return &CurrentUserPermissionChecker{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (c *CurrentUserPermissionChecker) IsGranted(policy string) bool {
	//fmt.Println("GlobalTokenClaims：",token.GlobalTokenClaims)
	userId := token.GetCurrentUserId(c.GinCtx)
	if len(userId) <= 0 {
		return false
	}
	return NewDSAuthChecker(c.GinCtx).IsUserGranted(userId, policy)
}
