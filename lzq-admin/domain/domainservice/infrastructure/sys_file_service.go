package infra_domainservice

import (
	"bytes"
	"errors"
	"io"
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	infra_extrastruct "lzq-admin/domain/model/infrastructure/extrastruct"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"
	"mime/multipart"
	"path"
	"strings"
	"sync"
	"time"

	"github.com/gin-gonic/gin"
	jsoniter "github.com/json-iterator/go"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/5/15
 * @Version 1.0.0
 */

type SysFileDomainService struct {
	domainservice.BaseDomainService
	wg sync.WaitGroup
}

func NewDSSysFile(c *gin.Context) *SysFileDomainService {
	return &SysFileDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
		sync.WaitGroup{},
	}
}

func (s *SysFileDomainService) Insert(status string, file multipart.File, header multipart.FileHeader) (infra_model.SystemFile, error) {
	var entity infra_model.SystemFile
	entity.ID = utility.UuidCreate()
	entity.OriginalName = header.Filename
	entity.Size = header.Size
	entity.ContentType = header.Header["Content-Type"][0]
	entity.Extension = strings.ToLower(path.Ext(header.Filename)) // 去文件后缀
	entity.NewName = entity.ID + entity.Extension
	if len(status) > 0 {
		entity.Status = domainconsts.SysFileStatusInuse
	} else {
		entity.Status = domainconsts.SysFileStatusUnused
	}
	buf := bytes.NewBuffer(nil)
	if _, err := io.Copy(buf, file); err != nil {
		return entity, err
	}
	if config, err := NewDSSysConfig(s.GinCtx).GetSysConfigCacheByCode(infra_model.ExtraQiNiuConfig, "QiNiuStorage"); err != nil {
		return entity, err
	} else {
		// 上传七牛云
		var qnconfig infra_extrastruct.ExtraQiNiuConfig
		if err := jsoniter.UnmarshalFromString(config, &qnconfig); err != nil {
			return entity, err
		}
		fileUrl := utility.UrlJoint(qnconfig.Directory, time.Now().Format("20060102"), entity.NewName)
		entity.Url = strings.TrimPrefix(fileUrl, qnconfig.Directory)
		_, entity.ThirdPartyId, err = utility.QiniuUtil.UploadQiNiuByStream(fileUrl, buf.Bytes(),
			utility.QiniuConfig{
				QiNiuBucket:    qnconfig.Bucket,
				QiNiuSecretKey: qnconfig.SecretKey,
				QiNiuAccessKey: qnconfig.AccessKey,
				QiNiuArea:      qnconfig.Area,
				QiNiuBaseUrl:   qnconfig.BaseUrl,
			})
		if err != nil {
			return entity, err
		}
	}
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	if _, err := lzqOrm.ISession().Insert(&entity); err != nil {
		return infra_model.SystemFile{}, err
	}
	return entity, nil
}

func (s *SysFileDomainService) UpdateStatus(id, status string) (infra_model.SystemFile, error) {
	var entity infra_model.SystemFile
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	isExit, err := lzqOrm.QSession(true).ID(id).Get(&entity)
	if err != nil {
		return entity, err
	}
	if !isExit {
		return entity, errors.New("该文件不已存在")
	}
	entity.Status = status
	if status == domainconsts.SysFileStatusDeleted {
		if _, err := lzqOrm.DSession(true).ID(entity.ID).Update(&entity); err != nil {
			return infra_model.SystemFile{}, err
		}
	} else {
		if _, err := lzqOrm.USession(true).ID(entity.ID).Update(&entity); err != nil {
			return infra_model.SystemFile{}, err
		}
	}
	return entity, nil
}
