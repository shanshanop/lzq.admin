/*
 * @Author: 糊涂的老知青
 * @Date: 2022-05-16
 * @Version: 1.0.0
 * @Description:
 */
package infra_domainservice

import (
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"

	"github.com/gin-gonic/gin"
)

type LogAuditLogService struct {
	domainservice.BaseDomainService
}

func NewDSLogAuditLog(c *gin.Context) *LogAuditLogService {
	return &LogAuditLogService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *LogAuditLogService) Insert(inputDto infra_model.CreateLogAuditLogDto) (infra_model.LogAuditLogDto, error) {
	var result infra_model.LogAuditLogDto
	return result, nil
}
