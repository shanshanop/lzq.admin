package infra_domainservice

import (
	"encoding/json"
	"fmt"
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	"lzq-admin/domain/dto"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/cache"
	"lzq-admin/pkg/orm"
	"strings"
	"sync"

	"github.com/gin-gonic/gin"

	"github.com/ahmetb/go-linq/v3"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/2/27
 * @Version 1.0.0
 */

type AuthCheckerDomainService struct {
	domainservice.BaseDomainService
	wg sync.WaitGroup
}

func NewDSAuthChecker(c *gin.Context) *AuthCheckerDomainService {
	return &AuthCheckerDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
		sync.WaitGroup{},
	}
}

func (d *AuthCheckerDomainService) GetRoleGrantedPermissions(roleId string) ([]dto.RoleGrantPermissionDto, error) {
	result := make([]dto.RoleGrantPermissionDto, 0)
	cacheKey := cache.NewLzqCacheKey(d.GinCtx).GetRoleGrantedPermissionsCacheKey(roleId)
	cacheJson := cache.RedisUtil.NewRedis(d.GinCtx, true).Get(cacheKey)
	if cacheJson != "" {
		_ = json.Unmarshal([]byte(cacheJson), &result)
		return result, nil
	}
	lzqOrm := orm.NewLzqOrm(d.GinCtx)
	dbSession := lzqOrm.QSession(true, "rp").
		Table(infra_model.TableAuthRolePermission).Alias("rp").
		Join("INNER", infra_model.TableAuthPermission+" as p", lzqOrm.ConditionWithDeletedOrTenantId(false, "rp.PermissionId=p.Id", "p")).
		Join("INNER", infra_model.TableAuthMenu+" as m", lzqOrm.ConditionWithDeletedOrTenantId(false, "p.MenuId=m.Id", "m")).
		Select("rp.Id,rp.RoleId,CONCAT(m.Policy,':',p.Policy) as Policy,rp.IsGranted").
		Where("rp.RoleId=? and rp.IsGranted=?", roleId, true)
	if err := dbSession.Find(&result); err != nil {
		return result, err
	}

	cache.RedisUtil.NewRedis(d.GinCtx, true).SSet(cacheKey, result, 0)
	return result, nil
}

func (d *AuthCheckerDomainService) GetGrantedDataPrivilegesByUser(userId string) ([]dto.UserDataPrivilegeDto, error) {
	result := make([]dto.UserDataPrivilegeDto, 0)
	cacheKey := cache.NewLzqCacheKey(d.GinCtx).GetGrantedDataPrivilegeByUserCacheKey(userId)
	cacheJson := cache.RedisUtil.NewRedis(d.GinCtx, true).Get(cacheKey)
	if cacheJson != "" {
		_ = json.Unmarshal([]byte(cacheJson), &result)
		return result, nil
	}
	dbSession := orm.NewLzqOrm(d.GinCtx).QSession(true, "u", "udp", "r").
		Table(infra_model.TableSystemUser).Alias("u").
		Join("INNER", infra_model.TableAuthUserdataprivilege+" as udp", "u.Id=udp.UserId").
		Join("INNER", infra_model.TableAuthRole+" as r", "udp.RoleId=r.Id").
		Select("udp.Id,udp.RoleId,udp.UserId").
		Where("udp.UserId=? and u.Status=? and r.RoleStatus=?", userId, domainconsts.SystemUserStatusEnable, domainconsts.RoleStatusEnable)
	if err := dbSession.Find(&result); err != nil {
		return result, err
	}

	cache.RedisUtil.NewRedis(d.GinCtx, true).SSet(cacheKey, result, 0)
	return result, nil
}

func (d *AuthCheckerDomainService) GetUserGrantedRoleIds(userId string) ([]string, error) {
	if userDataPrivileges, err := d.GetGrantedDataPrivilegesByUser(userId); err != nil {
		return nil, err
	} else {
		roleIds := make([]string, 0)
		linq.From(userDataPrivileges).SelectT(func(s dto.UserDataPrivilegeDto) string {
			return s.RoleID
		}).Distinct().ToSlice(&roleIds)
		return roleIds, nil
	}
}

func (d *AuthCheckerDomainService) IsUserGranted(userId, policy string) bool {
	if len(policy) == 0 {
		return true
	}
	// 租户超级管理员不用校验权限
	if isSuperAdmin, err := NewDSSystemUser(d.GinCtx).IsSuperAdmin(userId); err != nil {
		return false
	} else if isSuperAdmin {
		return true
	}
	actualPolicy := infra_model.GetActualPolicy(policy)

	cacheKey := cache.NewLzqCacheKey(d.GinCtx).GetUserGrantedPolicyCacheKey(userId)
	cacheJson := cache.RedisUtil.NewRedis(d.GinCtx, true).HGet(cacheKey, actualPolicy)
	fmt.Println(policy, cacheJson)
	if cacheJson != "" {
		return cacheJson == "1"
	}
	if roleIds, err := d.GetUserGrantedRoleIds(userId); err != nil {
		return false
	} else {
		for _, v := range roleIds {
			roleGrantedPermissions, _ := d.GetRoleGrantedPermissions(v)
			isGranted := linq.From(roleGrantedPermissions).AnyWithT(func(w dto.RoleGrantPermissionDto) bool {
				return strings.ToLower(w.Policy) == strings.ToLower(actualPolicy)
			})
			if isGranted {
				cache.RedisUtil.NewRedis(d.GinCtx, true).HSet(cacheKey, actualPolicy, true, 0)
				return true
			}
		}
	}
	cache.RedisUtil.NewRedis(d.GinCtx, true).HSet(cacheKey, actualPolicy, false, 0)
	return false
}

func (d *AuthCheckerDomainService) GetUserGrantedPermissions(userId string) ([]string, error) {
	result := make([]string, 0)
	cacheKey := cache.NewLzqCacheKey(d.GinCtx).GetUserGrantedPermissionsCacheKey(userId)
	cacheJson := cache.RedisUtil.NewRedis(d.GinCtx, true).Get(cacheKey)
	if cacheJson != "" {
		_ = json.Unmarshal([]byte(cacheJson), &result)
		return result, nil
	}
	if userGrantedRoleIds, err := d.GetUserGrantedRoleIds(userId); err != nil {
		return nil, err
	} else {
		for _, v := range userGrantedRoleIds {
			if roleGrantedPermissions, err := d.GetRoleGrantedPermissions(v); err != nil {
				return nil, err
			} else if len(roleGrantedPermissions) > 0 {
				p := make([]string, 0)
				linq.From(roleGrantedPermissions).WhereT(func(w dto.RoleGrantPermissionDto) bool {
					return w.IsGranted
				}).SelectT(func(s dto.RoleGrantPermissionDto) string {
					return s.Policy
				}).Distinct().ToSlice(&p)
				if len(p) > 0 {
					result = append(result, p...)
				}
			}
		}
	}
	cache.RedisUtil.NewRedis(d.GinCtx, true).SSet(cacheKey, result, 0)
	return result, nil
}
