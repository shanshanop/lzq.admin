/*
 * @Author: 糊涂的老知青
 * @Date: 2022-05-23
 * @Version: 1.0.0
 * @Description:
 */
package infra_domainservice

import (
	"errors"
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"

	"github.com/gin-gonic/gin"
)

type SystemCompanyDomainService struct {
	domainservice.BaseDomainService
}

func NewDSSystemCompany(c *gin.Context) *SystemCompanyDomainService {
	return &SystemCompanyDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *SystemCompanyDomainService) Insert(inputDto infra_model.CreateSystemCompanyDto) (infra_model.SystemCompany, error) {
	var entity infra_model.SystemCompany
	entity.SystemCompanyBase = inputDto.SystemCompanyBase
	entity.ID = utility.UuidCreate()
	if p, err := orm.NewLzqOrm(s.GinCtx).ISession().Insert(&entity); err != nil {
		return entity, err
	} else if p <= 0 {
		return entity, errors.New("新增公司失败")
	}
	return entity, nil
}

func (s *SystemCompanyDomainService) Update(inputDto infra_model.UpdateSystemCompanyDto) (infra_model.SystemCompany, error) {
	var entity infra_model.SystemCompany
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	if has, err := lzqOrm.QSession(true).ID(inputDto.ID).Get(&entity); err != nil {
		return entity, err
	} else if !has {
		return entity, errors.New("该公司ID不存在")
	}

	entity.SystemCompanyBase = inputDto.SystemCompanyBase
	if p, err := lzqOrm.USession(true).AllCols().ID(inputDto.ID).Update(&entity); err != nil {
		return entity, err
	} else if p <= 0 {
		return entity, errors.New("修改公司失败")
	}
	return entity, nil
}

func (s *SystemCompanyDomainService) Delete(id string) error {
	var entity infra_model.SystemCompany
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	if has, err := lzqOrm.QSession(true).ID(id).Get(&entity); err != nil {
		return err
	} else if !has {
		return errors.New("该公司ID不存在")
	}
	if p, err := lzqOrm.DSession(true).ID(id).Update(&entity); err != nil {
		return err
	} else if p <= 0 {
		return errors.New("删除公司失败")
	}
	return nil
}
