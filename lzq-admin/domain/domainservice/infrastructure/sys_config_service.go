/**
 * @Author  糊涂的老知青
 * @Date    2022/3/29
 * @Version 1.0.0
 */
package infra_domainservice

import (
	"errors"
	"fmt"
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	infra_extrastruct "lzq-admin/domain/model/infrastructure/extrastruct"
	"lzq-admin/pkg/cache"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"

	"github.com/gin-gonic/gin"

	jsoniter "github.com/json-iterator/go"
)

type SysConfigDomainService struct {
	domainservice.BaseDomainService
}

func NewDSSysConfig(c *gin.Context) *SysConfigDomainService {
	return &SysConfigDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *SysConfigDomainService) Insert(inputDto infra_model.CreateSystemConfigDto) (infra_model.SystemConfig, error) {
	var systemConfig infra_model.SystemConfig
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	isExit, err := lzqOrm.QSession(true).Where("ConfigType=? and Code=?", inputDto.ConfigType, inputDto.Code).Exist(&systemConfig)
	if err != nil {
		return systemConfig, err
	}
	if isExit {
		return systemConfig, errors.New("该配置类型已存在该配置编码：" + inputDto.Code)
	}

	systemConfig.ID = utility.UuidCreate()
	systemConfig.ConfigType = inputDto.ConfigType
	systemConfig.Code = inputDto.Code
	systemConfig.Name = inputDto.Name
	systemConfig.ExtraProperties = make(map[string]interface{})
	systemConfig.ExtraProperties[infra_model.ExtraSysConfigKey] = infra_model.ConfigTypeConstStruct[inputDto.ConfigType] //s.GetExtraPropertiesJson(inputDto.ConfigType) //new(extrastruct.ExtraQiNiuConfig)
	systemConfig.Status = domainconsts.CommonStatusEnable
	if _, err := lzqOrm.ISession().Insert(&systemConfig); err != nil {
		return infra_model.SystemConfig{}, err
	}
	return systemConfig, nil
}

func (s *SysConfigDomainService) Update(inputDto infra_model.UpdateSystemConfigDto) (infra_model.SystemConfig, error) {
	var systemConfig infra_model.SystemConfig
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	isExit, err := lzqOrm.QSession(false).Where("ConfigType=? and Code=?", inputDto.ConfigType, inputDto.Code).Get(&systemConfig)
	if err != nil {
		return systemConfig, err
	}
	if !isExit {
		return systemConfig, errors.New("该配置类型不已存在：" + inputDto.Code)
	}
	systemConfig.ExtraProperties[infra_model.ExtraSysConfigKey] = inputDto.ExtraValue
	if _, err := lzqOrm.USession(true).AllCols().ID(systemConfig.ID).Update(&systemConfig); err != nil {
		return infra_model.SystemConfig{}, err
	}
	r := cache.RedisUtil.NewRedis(s.GinCtx, false, "SysConfig")
	r.Delete(fmt.Sprintf("%v:%v", systemConfig.ConfigType, systemConfig.Code))
	return systemConfig, nil
}

func (s *SysConfigDomainService) GetByCode(configType, code string) (infra_model.SystemConfig, error) {
	var systemConfig infra_model.SystemConfig
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	isExit, err := lzqOrm.QSession(false).Where("ConfigType=? and Code=?", configType, code).Get(&systemConfig)
	if err != nil {
		return systemConfig, err
	}
	if !isExit {
		return systemConfig, errors.New("不存在该配置")
	}

	return systemConfig, nil
}

func (s *SysConfigDomainService) GetSysConfigCacheByCode(configType, code string) (string, error) {
	r := cache.RedisUtil.NewRedis(s.GinCtx, false, "SysConfig")
	key := fmt.Sprintf("%v:%v", configType, code)
	obj := r.Get(key)
	if obj != "" {
		return obj, nil
	}
	var systemConfig infra_model.SystemConfig
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	isExit, err := lzqOrm.QSession(false).Where("ConfigType=? and Code=?", configType, code).Get(&systemConfig)
	if err != nil {
		return "", err
	}
	if !isExit {
		return "", errors.New("不存在该配置")
	}
	v := systemConfig.ExtraProperties[infra_model.ExtraSysConfigKey]
	r.SSet(key, v, 0)
	json, _ := jsoniter.MarshalToString(v)
	return json, nil
}

func (s *SysConfigDomainService) GetExtraPropertiesJson(configType string) interface{} {
	var json interface{}
	switch configType {
	case infra_model.ExtraString:
		json = ""
		break
	case infra_model.ExtraInt:
		json = 0
		break
	case infra_model.ExtraStringArray:
		json = make([]string, 0)
		break
	case infra_model.ExtraIntArray:
		json = make([]int, 0)
		break
	case infra_model.ExtraQiNiuConfig:
		json = new(infra_extrastruct.ExtraQiNiuConfig)
		break
	default:
		json = s.SetSysConfigJsonMapCache(configType)
		break
	}
	return json
}

func (s *SysConfigDomainService) SetSysConfigJsonMapCache(configType string) interface{} {
	c := cache.RedisUtil.NewRedis(s.GinCtx, false)
	var objJson interface{}
	isAllConfigType := configType == "AllConfigType"
	if !isAllConfigType {
		objJson = c.HGet("ExtraStruct:KeyJson", configType)
		if objJson != nil {
			return objJson
		}
	}
	var obj infra_extrastruct.ExtraConfigObject
	jsonMap := infra_model.ReflectSysconfigJsonMap(obj)
	if isAllConfigType {
		for k, v := range jsonMap {
			vm := v.(map[string]interface{})
			for kk, vv := range vm {
				c.HSet(fmt.Sprintf("ExtraStruct:%v", kk), k, vv, 0)
			}
		}
		objJson = jsonMap
	} else {
		if v, ok := jsonMap[configType]; !ok {
			panic("该配置类型：" + configType + " 不存在")

		} else {
			vm := v.(map[string]interface{})
			objJson = vm["KeyJson"]
		}
	}

	return objJson
}
