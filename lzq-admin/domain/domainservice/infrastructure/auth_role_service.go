/*
 * @Author: 糊涂的老知青
 * @Date: 2021-12-04
 * @Version: 1.0.0
 * @Description:
 */
package infra_domainservice

import (
	"errors"
	"lzq-admin/domain/domainconsts"
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"

	"github.com/gin-gonic/gin"
)

type AuthRoleDomainService struct {
	domainservice.BaseDomainService
}

func NewDSAuthRole(c *gin.Context) *AuthRoleDomainService {
	return &AuthRoleDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *AuthRoleDomainService) Insert(modelDto infra_model.CreateAuthRoleDto) (infra_model.AuthRole, error) {
	var entity infra_model.AuthRole
	var exist bool
	var err error

	exist, err = orm.NewLzqOrm(s.GinCtx).QSession(true).Where("Name=?", modelDto.Name).Get(&infra_model.AuthRole{})
	if err != nil {
		return entity, err
	}
	if exist {
		return entity, errors.New("角色名称：" + modelDto.Name + " 已存在，请更换")
	}
	entity = infra_model.AuthRole{
		AuthRoleBase: infra_model.AuthRoleBase{
			Name:        modelDto.Name,
			Code:        modelDto.Code,
			Description: modelDto.Description,
			RoleStatus:  domainconsts.RoleStatusEnable,
		},
	}
	entity.ID = utility.UuidCreate()
	_, err = orm.NewLzqOrm(s.GinCtx).ISession().Insert(&entity)
	return entity, err
}
