/*
 * @Author: 糊涂的老知青
 * @Date: 2022-02-16
 * @Version: 1.0.0
 * @Description:
 */
package infra_domainservice

import (
	"lzq-admin/domain/domainservice"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/orm"
	"lzq-admin/pkg/utility"

	"github.com/ahmetb/go-linq/v3"
	"github.com/gin-gonic/gin"
	"xorm.io/xorm"
)

// AuthUserdataPrivilegeDomainService AuthPermissionDomainService 操作权限领域服务
type AuthUserdataPrivilegeDomainService struct {
	domainservice.BaseDomainService
}

func NewDSAuthUserdataPrivilege(c *gin.Context) *AuthUserdataPrivilegeDomainService {
	return &AuthUserdataPrivilegeDomainService{
		domainservice.BaseDomainService{
			GinCtx: c,
		},
	}
}

func (s *AuthUserdataPrivilegeDomainService) Insert(dbSession *xorm.Session, userId string, roleIds []string) (result []infra_model.AuthUserDataPrivilege, err error) {
	entities := make([]infra_model.AuthUserDataPrivilege, 0)
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	if err := lzqOrm.QSession(true).Where("UserId = ?", userId).Find(&entities); err != nil {
		return nil, err
	}
	// 新增
	for _, roleId := range roleIds {
		//var exitEntity infra_model.AuthUserDataPrivilege
		exitIndex := linq.From(entities).IndexOfT(func(w infra_model.AuthUserDataPrivilege) bool {
			return w.RoleId == roleId
		})
		// 将已经存在的过滤掉，没有选择的则是需要删除
		if exitIndex >= 0 {
			entities = append(entities[:exitIndex], entities[exitIndex+1:]...)
			continue
		}
		entity := infra_model.AuthUserDataPrivilege{
			AuthUserDataPrivilegeBase: infra_model.AuthUserDataPrivilegeBase{
				RoleId: roleId,
				UserId: userId,
			},
		}
		entity.ID = utility.UuidCreate()
		_, err = lzqOrm.ISessionWithTrans(dbSession).Insert(&entity)
		if err != nil {
			dbSession.Rollback()
			return nil, err
		}
		result = append(result, entity)
	}

	for _, ud := range entities {
		_, err = lzqOrm.DSessionWithTrans(true, dbSession).ID(ud.ID).Update(&infra_model.AuthUserDataPrivilege{})
		if err != nil {
			dbSession.Rollback()
			return nil, err
		}
	}
	if len(result) > 0 {
		NewDSAuthPrivilege(s.GinCtx).DeleteUserAuthCache(userId)
	}
	return result, nil
}

func (s *AuthUserdataPrivilegeDomainService) Delete(dbSession *xorm.Session, userId string, roleId string) error {
	m := infra_model.AuthUserDataPrivilege{}
	lzqOrm := orm.NewLzqOrm(s.GinCtx)
	if len(roleId) > 0 {
		if _, err := lzqOrm.DSessionWithTrans(true, dbSession).Where("UserId = ? and RoleId=?", userId, roleId).Update(&m); err != nil {
			dbSession.Rollback()
			return err
		}
	} else {
		if _, err := lzqOrm.DSessionWithTrans(true, dbSession).Where("UserId = ?", userId).Update(&m); err != nil {
			dbSession.Rollback()
			return err
		}
	}
	return nil
}
