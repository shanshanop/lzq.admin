/*
 * @Author: 糊涂的老知青
 * @Date: 2021-11-30
 * @Version: 1.0.0
 * @Description:
 */
package tenant_application

import (
	"fmt"
	"lzq-admin/application"
	tenant_domainservice "lzq-admin/domain/domainservice/tenant"
	tenant_model "lzq-admin/domain/model/tenant"

	"github.com/gin-gonic/gin"
)

type TenantAppService struct {
	application.BaseAppService
}

var ITenantAppService = TenantAppService{}

// Create 测试接口
// @Summary 创建租户
// @Tags Tenant
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body tenant_model.CreateTenantDto true " "
// @Success 200 object tenant_model.CreateTenantDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/tenant/tenant/create [POST]
func (tenant *TenantAppService) Create(c *gin.Context) {
	var inputDto tenant_model.CreateTenantDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		// gin.H封装了生成json数据的工具
		tenant.ResponseError(c, err)
		return
	}

	result, rerr := tenant_domainservice.NewDSTenant(c).Insert(inputDto)
	if rerr != nil {
		// gin.H封装了生成json数据的工具
		tenant.ResponseError(c, rerr)
		return
	}
	fmt.Println(result)
	tenant.ResponseSuccess(c, inputDto)
}
