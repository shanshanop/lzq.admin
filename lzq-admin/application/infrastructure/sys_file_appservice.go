/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */
package infra_application

import (
	"lzq-admin/application"
	"lzq-admin/domain/domainconsts"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	infra_model "lzq-admin/domain/model/infrastructure"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/5/15
 * @Version 1.0.0
 */

type sysFileAppService struct {
	application.BaseAppService
}

var ISysFileAppService = sysFileAppService{}

// Upload
// @Summary 单个上传文件
// @Tags SysFile
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param file formData file true "文件"
// @Success 200 {object} infra_model.SystemFile " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysfile/upload [POST]
func (app *sysFileAppService) Upload(c *gin.Context) {
	file, header, err := c.Request.FormFile("file")
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	rep, err := infra_domainservice.NewDSSysFile(c).Insert(domainconsts.SysFileStatusInuse, file, *header)
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	//var rep infra_model.SystemFile
	app.ResponseSuccess(c, rep)
}

// BatchUpload
// @Summary 批量上传文件
// @Tags SysFile
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param files formData file true "文件"
// @Success 200 {array} infra_model.SystemFile " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysfile/batchUpload [POST]
func (app *sysFileAppService) BatchUpload(c *gin.Context) {
	form, err := c.MultipartForm()
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	files := form.File["files"]
	reps := make([]infra_model.SystemFile, 0)
	for _, header := range files {
		f, err := header.Open()
		rep, err := infra_domainservice.NewDSSysFile(c).Insert(domainconsts.SysFileStatusInuse, f, *header)
		if err != nil {
			app.ResponseError(c, err)
			return
		}
		reps = append(reps, rep)
	}

	app.ResponseSuccess(c, reps)
}
