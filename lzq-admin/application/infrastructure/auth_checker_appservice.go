package infra_application

import (
	"encoding/json"
	"errors"
	"lzq-admin/application"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	infra_model "lzq-admin/domain/model/infrastructure"
	token "lzq-admin/pkg/auth"
	"lzq-admin/pkg/cache"
	"lzq-admin/pkg/orm"
	"sort"

	"github.com/ahmetb/go-linq/v3"
	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/1/30
 * @Version 1.0.0
 */

type authCheckerAppService struct {
	application.BaseAppService
}

var IAuthCheckerAppService = authCheckerAppService{}

// GetGrantedMenus doc
// @Summary 查询当前用户已授权的菜单
// @Tags AuthChecker
// @Description
// @Security ApiKeyAuth
// @Produce  json
// @Success 200 {array} infra_model.UserGrantedMenuDto “ ”
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/authenticateChecker/grantedMenus [GET]
func (app *authCheckerAppService) GetGrantedMenus(c *gin.Context) {
	userId := token.GetCurrentUserId(c)
	result := make([]infra_model.UserGrantedMenuDto, 0)

	cacheKey := cache.NewLzqCacheKey(c).GetUserGrantedMenusCacheKey(userId)
	cacheJson := cache.RedisUtil.NewRedis(c, true).Get(cacheKey)
	if cacheJson != "" {
		_ = json.Unmarshal([]byte(cacheJson), &result)
		app.ResponseSuccess(c, result)
		return
	}
	var menus = make([]infra_model.UserGrantedMenuDto, 0)
	dbSession := orm.NewLzqOrm(c).QSession(false, "menu").
		Table(infra_model.TableAuthMenu).Alias("menu").
		Omit("Children").
		Where("menu.IsHidden = ?", false)
	if err := dbSession.Find(&menus); err != nil {
		app.ResponseError(c, err)
		return
	}
	pMenus := make([]infra_model.UserGrantedMenuDto, 0)
	linq.From(menus).WhereT(func(s infra_model.UserGrantedMenuDto) bool {
		return s.IsBranch && len(s.ParentId) == 0
	}).ToSlice(&pMenus)

	if isSuperAdmin, err := infra_domainservice.NewDSSystemUser(c).IsSuperAdmin(userId); err != nil {
		app.ResponseError(c, err)
		return
	} else if isSuperAdmin {
		result = grantedMenuTree(pMenus, menus)
	} else {
		rightMenus := make([]infra_model.UserGrantedMenuDto, 0)
		for _, m := range menus {
			if len(m.Policy) > 0 {
				if isGranted := infra_domainservice.NewDSCurrentUserPermission(c).IsGranted(m.Policy); isGranted {
					rightMenus = append(rightMenus, m)
				}
			} else {
				rightMenus = append(rightMenus, m)
			}
		}
		result = grantedMenuTree(pMenus, rightMenus)
	}
	cache.RedisUtil.NewRedis(c, true).SSet(cacheKey, result, 0)
	app.ResponseSuccess(c, result)
}
func grantedMenuTree(parentMenus []infra_model.UserGrantedMenuDto, menus []infra_model.UserGrantedMenuDto) []infra_model.UserGrantedMenuDto {
	// 排序 -- 升序 由小到大  使用大于号>表示降序，小于号<表示升序
	sort.SliceStable(parentMenus, func(i int, j int) bool {
		return parentMenus[i].Rank < parentMenus[j].Rank
	})
	pMenus := make([]infra_model.UserGrantedMenuDto, 0)
	for i := 0; i < len(parentMenus); i++ {
		cMenus := make([]infra_model.UserGrantedMenuDto, 0)
		linq.From(menus).WhereT(func(s infra_model.UserGrantedMenuDto) bool {
			return s.ParentId == parentMenus[i].Id
		}).ToSlice(&cMenus)
		if len(cMenus) > 0 {
			parentMenus[i].Children = grantedMenuTree(cMenus, menus)
		}

		if parentMenus[i].IsBranch && len(parentMenus[i].Children) > 0 {
			pMenus = append(pMenus, parentMenus[i])
		}
		if !parentMenus[i].IsBranch {
			pMenus = append(pMenus, parentMenus[i])
		}
	}
	return pMenus
}

// GetCurrentUserGrantedPermissions doc
// @Summary 查询当前用户所授权的操作权限
// @Tags AuthChecker
// @Description
// @Security ApiKeyAuth
// @Produce  json
// @Success 200 {array} string " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/permissionChecker/grantedPermissions [GET]
func (app *authCheckerAppService) GetCurrentUserGrantedPermissions(c *gin.Context) {
	if permissions, err := infra_domainservice.NewDSAuthChecker(c).GetUserGrantedPermissions(token.GetCurrentUserId(c)); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, permissions)
	}
}

// DeleteUserRole doc
// @Summary 删除用户数据授权
// @Tags AuthChecker
// @Description
// @Security ApiKeyAuth
// @Produce  json
// @Param userDataPrivilegeId query string true "用户数据授权ID"
// @Success 200 {object} application.ResponseDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/authorize/userRole [DELETE]
func (app *authCheckerAppService) DeleteUserRole(c *gin.Context) {
	userDataPrivilegeId := c.Query("userDataPrivilegeId")
	var dataPrivilege infra_model.AuthUserDataPrivilege
	lzqOrm := orm.NewLzqOrm(c)
	if has, err := lzqOrm.QSession(true).ID(userDataPrivilegeId).Get(&dataPrivilege); err != nil {
		app.ResponseError(c, err)
		return
	} else if !has {
		app.ResponseError(c, errors.New("该角色没有授权给该用户"))
		return
	}
	if isSuperAdmin, err := infra_domainservice.NewDSSystemUser(c).IsSuperAdmin(dataPrivilege.UserId); err != nil {
		app.ResponseError(c, err)
		return
	} else if isSuperAdmin {
		app.ResponseError(c, errors.New("管理员账号不能进行更换角色"))
		return
	}
	num, err := lzqOrm.DSession(true).ID(userDataPrivilegeId).Update(&infra_model.AuthUserDataPrivilege{})
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	if num <= 0 {
		app.ResponseError(c, errors.New("删除失败"))
		return
	}
	infra_domainservice.NewDSAuthPrivilege(c).DeleteFunctionPrivilegeCache()
	infra_domainservice.NewDSAuthPrivilege(c).DeleteDataPrivilegeCache()
	app.ResponseSuccess(c, true)
}
