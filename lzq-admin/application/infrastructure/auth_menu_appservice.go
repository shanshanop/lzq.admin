package infra_application

import (
	"lzq-admin/application"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	"lzq-admin/domain/dto"
	infra_model "lzq-admin/domain/model/infrastructure"
	"lzq-admin/pkg/orm"
	"sort"
	"strings"
	"sync"

	"github.com/ahmetb/go-linq/v3"
	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2021/12/1
 * @Version 1.0.0
 */

type authMenuAppService struct {
	application.BaseAppService
	wg sync.WaitGroup
}

var IAuthMenuAppService = authMenuAppService{}

// Create doc
// @Summary 创建菜单
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.CreateAuthMenuDto true " "
// @Success 200 {object} infra_model.AuthMenuDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/menu [POST]
func (app *authMenuAppService) Create(c *gin.Context) {
	var inputDto infra_model.CreateAuthMenuDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	result, err := infra_domainservice.NewDSAuthMenu(c).Insert(inputDto)
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	app.ResponseSuccess(c, result)
}

// Get doc
// @Summary 根据ID查询菜单
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth id
// @Accept mpfd
// @Produce  json
// @Param id query string true "菜单ID"
// @Success 200 {object} infra_model.AuthMenuDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/get [GET]
func (app *authMenuAppService) Get(c *gin.Context) {
	id := c.Query("id")
	if result, err := infra_domainservice.NewDSAuthMenu(c).Get(id); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, result)
	}
}

// Update doc
// @Summary 修改菜单
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.UpdateAuthMenuDto true " "
// @Success 200 {object} application.ResponseDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/menu [PUT]
func (app *authMenuAppService) Update(c *gin.Context) {
	var inputDto infra_model.UpdateAuthMenuDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}

	if _, err := infra_domainservice.NewDSAuthMenu(c).Update(inputDto); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, true)
	}
}

// Delete
// @Summary 删除菜单
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth 删除菜单，同时删除菜单下的操作权限
// @Produce  json
// @Param id query string true "菜单ID"
// @Success 200 {object} application.ResponseDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/menu [DELETE]
func (app *authMenuAppService) Delete(c *gin.Context) {
	id := c.Query("MenuId")
	if err := infra_domainservice.NewDSAuthMenu(c).Delete(id); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, true)
	}
}

// GetList doc
// @Summary 菜单列表
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param object query application.PageParamsDto true " "
// @Success 200 {array} infra_model.AuthMenuListDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/menusList [GET]
func (app *authMenuAppService) GetList(c *gin.Context) {
	var inputDto application.PageParamsDto
	if err := c.ShouldBind(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}

	dbSession := orm.NewLzqOrm(c).QSession(false, "menu").Table(&infra_model.AuthMenu{}).Alias("menu").
		Select("menu.*,module.Code as ModuleCode,module.Name as ModuleName").
		Join("Inner", infra_model.TableAuthModule+" as module", "menu.ModuleId = module.Id").
		Where("module.IsDeleted = ?", 0)
	if err := application.DBCondition(inputDto, dbSession, "menu", infra_model.AuthMenuListDto{}); err != nil {
		app.ResponseError(c, err)
		return
	}
	var result = make([]infra_model.AuthMenuListDto, 0)
	var resultDto application.PageListDto
	var err error
	dbSession.Omit("Operation", "Children")
	if inputDto.RequireTotalCount {
		resultDto.TotalCount, err = dbSession.FindAndCount(&result)
	} else {
		err = dbSession.Find(&result)
	}
	if err != nil {
		app.ResponseError(c, err)
		return
	}

	operation := application.GetCurrentUserGrantedOperation(c, []dto.OperationDto{
		dto.GetOperationButton("Edit", "编辑", "SystemSetup.Menu:Operation.Edit"),
		dto.GetOperationButton("Delete", "删除", "SystemSetup.Menu:Operation.Delete"),
	})
	// 整理出parentId=null的根菜单和子菜单
	var pMenus = make([]infra_model.AuthMenuListDto, 0)
	var cMenus = make([]infra_model.AuthMenuListDto, 0)
	for _, v := range result {
		v.Operation = operation
		if len(v.ParentId) == 0 {
			pMenus = append(pMenus, v)
		} else {
			cMenus = append(cMenus, v)
		}
	}
	if len(inputDto.Filter) > 0 {
		pMenus = append(pMenus, cMenus...)
		resultDto.Data = pMenus
	} else {
		resultDto.Data = menuTree(pMenus, cMenus)
	}
	app.ResponseSuccess(c, resultDto)
}
func menuTree(parentMenus []infra_model.AuthMenuListDto, menus []infra_model.AuthMenuListDto) []infra_model.AuthMenuListDto {
	// 排序 -- 升序 由小到大  使用大于号>表示降序，小于号<表示升序
	sort.SliceStable(parentMenus, func(i int, j int) bool {
		return parentMenus[i].Rank < parentMenus[j].Rank
	})
	for i := 0; i < len(parentMenus); i++ {
		cMenus := make([]infra_model.AuthMenuListDto, 0)
		linq.From(menus).WhereT(func(s infra_model.AuthMenuListDto) bool {
			return s.ParentId == parentMenus[i].ID
		}).ToSlice(&cMenus)
		if len(cMenus) > 0 {
			parentMenus[i].Children = menuTree(cMenus, menus)
		} else {
			parentMenus[i].Children = []infra_model.AuthMenuListDto{}
		}
	}
	return parentMenus
}

// GetMenuList doc
// @Summary 根据菜单类型查询菜单
// @Tags AuthMenu
// @Description
// @Security ApiKeyAuth id
// @Accept mpfd
// @Produce  json
// @Param menuType query string true "菜单类型"
// @Success 200 {array} infra_model.AuthMenuDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/menu/menuList [GET]
func (app *authMenuAppService) GetMenuList(c *gin.Context) {
	menuType := c.Query("menuType")

	var result = make([]infra_model.AuthMenuDto, 0)
	dbSession := orm.NewLzqOrm(c).QSession(false)
	if strings.ToLower(menuType) == "branch" {
		dbSession.Where("IsBranch = ?", true)
	} else if strings.ToLower(menuType) == "menu" {
		dbSession.Where("IsBranch = ?", false)
	}
	if err := dbSession.Find(&result); err != nil {
		app.ResponseError(c, err)
	} else {
		app.ResponseSuccess(c, result)
	}
}
