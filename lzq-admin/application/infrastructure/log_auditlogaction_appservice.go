package infra_application

import (
	"lzq-admin/application"
	infra_model "lzq-admin/domain/model/infrastructure"
	token "lzq-admin/pkg/auth"
	"lzq-admin/pkg/orm"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/5/19
 * @Version 1.0.0
 */

type logAuditLogActionAppService struct {
	application.BaseAppService
}

var LogAuditLogActionAppService = logAuditLogActionAppService{}

// GetList doc
// @Summary 查询接口日志列表
// @Tags AuditLogAction
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param object query application.PageParamsDto true " "
// @Success 200 {array} infra_model.LogAuditLogActionListDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/auditLogAction/list [GET]
func (app *logAuditLogActionAppService) GetList(c *gin.Context) {
	var inputDto application.PageParamsDto
	if err := c.ShouldBind(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	var result = make([]infra_model.LogAuditLogActionListDto, 0)
	dbSession := orm.NewLzqOrm(c).QSession(false).Omit("Operation")
	if err := application.DBCondition(inputDto, dbSession, "", infra_model.LogAuditLogActionListDto{}); err != nil {
		app.ResponseError(c, err)
		return
	}
	var resultDto application.PageListDto
	var err error
	if inputDto.RequireTotalCount {
		resultDto.TotalCount, err = dbSession.FindAndCount(&result)
	} else {
		err = dbSession.Find(&result)
	}
	if err != nil {
		app.ResponseError(c, err)
		return
	}

	resultDto.Data = result
	app.ResponseSuccess(c, resultDto)
}

// GetCurrentUserLogsList doc
// @Summary 查询当前用户的登录日志列表
// @Tags AuditLogAction
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param object query application.PageParamsDto true " "
// @Success 200 {array} infra_model.LogAuditLogActionListDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/auditLogAction/currentUserLogsList [GET]
func (app *logAuditLogActionAppService) GetCurrentUserLogsList(c *gin.Context) {
	var inputDto application.PageParamsDto
	if err := c.ShouldBind(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	inputDto.Filter = "[[\"userId\",\"=\",\"" + token.GetCurrentUserId(c) + "\"],[\"actionType\",\"=\",\"Login\"]]"

	var result = make([]infra_model.LogAuditLogActionListDto, 0)
	dbSession := orm.NewLzqOrm(c).QSession(false).Omit("Operation")
	if err := application.DBCondition(inputDto, dbSession, "", infra_model.LogAuditLogActionListDto{}); err != nil {
		app.ResponseError(c, err)
		return
	}
	var resultDto application.PageListDto
	var err error
	if inputDto.RequireTotalCount {
		resultDto.TotalCount, err = dbSession.FindAndCount(&result)
	} else {
		err = dbSession.Find(&result)
	}
	if err != nil {
		app.ResponseError(c, err)
		return
	}

	resultDto.Data = result
	app.ResponseSuccess(c, resultDto)
}
