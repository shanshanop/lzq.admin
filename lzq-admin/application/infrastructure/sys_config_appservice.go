package infra_application

import (
	"lzq-admin/application"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	infra_model "lzq-admin/domain/model/infrastructure"
	"sync"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/4/5
 * @Version 1.0.0
 */

type systemConfigAppService struct {
	application.BaseAppService
	wg sync.WaitGroup
}

var ISystemConfigAppService = systemConfigAppService{}

// Create doc
// @Summary 系统配置创建
// @Tags SysConfig
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.CreateSystemConfigDto true " "
// @Success 200 {object} infra_model.SystemConfig " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysconfig/create [POST]
func (app *systemConfigAppService) Create(c *gin.Context) {
	var inputDto infra_model.CreateSystemConfigDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	result, err := infra_domainservice.NewDSSysConfig(c).Insert(inputDto)
	if err != nil {
		app.ResponseError(c, err)
		return
	}

	app.ResponseSuccess(c, result)
}

// GetInfo doc
// @Summary 七牛云配置详情
// @Tags SysConfig
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param configType query string true "配置类型"
// @Param code query string true "配置编码"
// @Success 200 {object} infra_model.QiNiuConfigDto " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysconfig/getInfo [GET]
func (app *systemConfigAppService) GetInfo(c *gin.Context) {
	code := c.Query("code")
	configType := c.Query("configType")
	var result infra_model.SystemConfigDto
	sysConfig, err := infra_domainservice.NewDSSysConfig(c).GetByCode(configType, code)
	if err != nil {
		app.ResponseError(c, err)
		return
	}
	result.ID = sysConfig.ID
	result.ConfigType = sysConfig.ConfigType
	result.Code = sysConfig.Code
	result.Name = sysConfig.Name
	result.ExtraValue = sysConfig.ExtraProperties[infra_model.ExtraSysConfigKey]

	app.ResponseSuccess(c, result)
}

// GetSysConfigJsonMapCache doc
// @Summary 设置系统配置Json及注解缓存
// @Tags SysConfig
// @Description
// @Security ApiKeyAuth
// @Produce  json
// @Param configType query string true "配置类型"
// @Success 200 {object} interface{} “ ”
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysconfig/getSysConfigCache [GET]
func (app *systemConfigAppService) GetSysConfigJsonMapCache(c *gin.Context) {
	configType := c.Query("configType")
	objJson := infra_domainservice.NewDSSysConfig(c).SetSysConfigJsonMapCache(configType)
	app.ResponseSuccess(c, objJson)
}

// QiuNiuUpdate doc
// @Summary 配置七牛云
// @Tags SysConfig
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.QiNiuConfigDto true " "
// @Success 200 {object} infra_model.SystemConfig " "
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/sysconfig/qnupdate [POST]
func (app *systemConfigAppService) QiuNiuUpdate(c *gin.Context) {
	var inputDto infra_model.QiNiuConfigDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}

	sysConfig := infra_model.UpdateSystemConfigDto{
		SystemConfigBase: infra_model.SystemConfigBase{
			//Name:       inputDto.SystemConfigBase.Name,
			ConfigType: inputDto.ConfigUpdateDtoBase.ConfigType,
			Code:       inputDto.ConfigUpdateDtoBase.Code,
		},
		ExtraValue: inputDto.ExtraValue,
	}
	_, err := infra_domainservice.NewDSSysConfig(c).Update(sysConfig)
	if err != nil {
		app.ResponseError(c, err)
		return
	}

	app.ResponseSuccess(c, true)
}
