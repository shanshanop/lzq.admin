/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */
package infra_application

import (
	"lzq-admin/application"
	infra_domainservice "lzq-admin/domain/domainservice/infrastructure"
	infra_model "lzq-admin/domain/model/infrastructure"

	"github.com/gin-gonic/gin"
)

/**
 * @Author  糊涂的老知青
 * @Date    2022/5/23
 * @Version 1.0.0
 */

type systemCompanyAppService struct {
	application.BaseAppService
}

var SystemCompanyAppService = systemCompanyAppService{}

// Create doc
// @Summary 新增公司
// @Tags SystemCompany
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.CreateSystemCompanyDto true " "
// @Success 200 {object} infra_model.SystemCompany 其他描述
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/company/create [POST]
func (app *systemCompanyAppService) Create(c *gin.Context) {
	var inputDto infra_model.CreateSystemCompanyDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	if result, err := infra_domainservice.NewDSSystemCompany(c).Insert(inputDto); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, result)
	}
}

// Update doc
// @Summary 修改公司
// @Tags SystemCompany
// @Description
// @Security ApiKeyAuth
// @Accept json
// @Produce  json
// @Param object body infra_model.UpdateSystemCompanyDto true " "
// @Success 200 {object} infra_model.SystemCompany 其他描述
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/company/update [PUT]
func (app *systemCompanyAppService) Update(c *gin.Context) {
	var inputDto infra_model.UpdateSystemCompanyDto
	if err := c.ShouldBindJSON(&inputDto); err != nil {
		app.ResponseError(c, err)
		return
	}
	if result, err := infra_domainservice.NewDSSystemCompany(c).Update(inputDto); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, result)
	}
}

// Delete
// @Summary 删除公司
// @Tags SystemCompany
// @Description
// @Security ApiKeyAuth
// @Produce  json
// @Param id query string true "公司ID"
// @Success 200 {object} application.ResponseDto
// @Failure 500 {object} application.ResponseDto
// @Router /api/app/company/delete [DELETE]
func (app *systemCompanyAppService) Delete(c *gin.Context) {
	id := c.Query("id")
	if err := infra_domainservice.NewDSSystemCompany(c).Delete(id); err != nil {
		app.ResponseError(c, err)
		return
	} else {
		app.ResponseSuccess(c, true)
	}
}
