/*
 * @Author: 糊涂的老知青
 * @Date: 2022-08-10
 * @Version: 1.0.0
 * @Description:
 */

package workflow_application

import (
	"lzq-admin/application"

	"github.com/gin-gonic/gin"
)

type DesignerAppService struct {
	application.BaseAppService
}

// @Summary 根据用户ID或者登陆名查询用户信息
// @Tags 接口分组名称
// @Description
// @Security ApiKeyAuth
// @Accept mpfd
// @Produce  json
// @Param object query application.PageParamsDto true " "
// @Success 200 {array} string
// @Failure 500 {object} application.ResponseDto
// @Router /api/workflow/desiger/list [GET]
func (app *DesignerAppService) GetList(c *gin.Context) {
	app.ResponseSuccess(c, "")
	return
}
